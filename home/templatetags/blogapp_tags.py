from django.template import Library, loader
from django.urls import resolve

from ..models import Tag

register = Library()

@register.simple_tag()
def post_date_url(post, blog_page):
    post_date = post.date
    url = blog_page.url + blog_page.reverse_subpage(
        'post_by_date_slug',
        args = (
            post_date.year,
            '{0:02}'.format(post_date.month),
            '{0:02}'.format(post_date.day),
            post.slug,
        )
    )
    return url


@register.inclusion_tag('home/components/tags_list.html',
                        takes_context=True)
def tags_list(context, limit=None):
    home_page = context['home_page']
    tags = Tag.objects.all()
    if limit:
        tags = tags[:limit]
    return {
        'home_page': home_page, 'request': context['request'], 'tags': tags
    }

