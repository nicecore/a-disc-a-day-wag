import datetime
from datetime import date

from django import forms
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.db import models
from django.utils.dateformat import DateFormat
from django.utils.formats import date_format

from modelcluster.fields import ParentalKey
from modelcluster.tags import ClusterTaggableManager

from taggit.models import TaggedItemBase, Tag as TaggitTag

from wagtail.admin.edit_handlers import FieldPanel
from wagtail.contrib.routable_page.models import RoutablePageMixin, route
from wagtail.core.fields import RichTextField
from wagtail.core.models import Page
# from wagtail.search import index
from wagtail.snippets.models import register_snippet

class HomePage(RoutablePageMixin, Page):
    description = models.CharField(max_length=255, blank=True, null=True)

    content_panels = Page.content_panels + [
        FieldPanel('description', classname='full')
    ]
    
    def get_context(self, request, *args, **kwargs):
        context = super(HomePage, self).get_context(request, *args, **kwargs)
        context['posts'], context['page_range'] = self.pagination(request, self.get_posts())
        # End attempt
        context['home_page'] = self
        context['search_type'] = getattr(self, 'search_type', '')
        context['search_term'] = getattr(self, 'search_term', '')
        return context


    def get_posts(self):
        return BlogPage.objects.descendant_of(self).live().order_by('-date')


    def pagination(self, request, posts):
        paginator = Paginator(posts, 5)
        page = request.GET.get('page')
        try:
            posts = paginator.page(page)
        except PageNotAnInteger:
            posts = paginator.page(1)
        except EmptyPage:
            posts = paginator.page(paginator.num_pages)
        index = posts.number - 1
        max_index = len(paginator.page_range)
        start_index = index - 5 if index >= 5 else 0
        end_index = index + 5 if index <= max_index - 5 else max_index
        page_range = paginator.page_range[start_index:end_index]
        print('\n', posts, '\n')
        return posts, page_range


    @route(r'^(\d{4}/$)')
    @route(r'^(\d{4})/(\d{2})/$')
    @route(r'^(\d{4})/(\d{2})/(\d{2})/$')
    def post_by_date(self, request, year, month=None,
                    day=None, *args, **kwargs):
        self.posts = self.get_posts().filter(date__year=year)
        self.search_type = 'date'
        self.search_term = year
        if month:
            self.posts = self.posts.filter(date__month=month)
            df = DateFormat(date(int(year), int(month), 1))
            self.search_term = df.format('F Y')
        if day:
            self.posts = self.posts.filter(date__day=day)
            self.search_term = date_format(date(int(year), int(month), int(day)))
        return Page.serve(self, request, *args, **kwargs)


    @route(r'^(\d{4})/(\d{2})/(\d{2})/(.+)/$')
    def post_by_date_slug(self, request, year, month, day,
                        slug, *args, **kwargs):
        blog_page = self.get_posts().filter(slug=slug).first()
        if not blog_page:
            raise Http404
        return Page.serve(blog_page, request, *args, **kwargs)


    @route(r'^tag/(?P<tag>[-\w]+)/$')
    def post_by_tag(self, request, tag, *args, **kwargs):
        self.search_type = 'tag'
        self.search_term = tag
        self.posts = self.get_posts().filter(tags__slug=tag)
        return Page.serve(self, request, *args, **kwargs)


    @route(r'^search/$')
    def post_search(self, request, *args, **kwargs):
        search_query = request.GET.get('q', None)
        self.posts = self.get_posts()
        if search_query:
            self.posts = self.posts.filter(body__icontains=search_query)
            self.search_term = search_query
            self.search_type = 'search'
        return Page.serve(self, request, *args, **kwargs)

    @route(r'^$')
    def post_list(self, request, *args, **kwargs):
        self.posts = self.get_posts()
        return Page.serve(self, request, *args, **kwargs)



class BlogPage(Page):
    date = models.DateField(verbose_name="Post date", default=datetime.datetime.today)
    summary = models.CharField(max_length=400)
    cover = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+',
    )
    body = RichTextField(blank=True)
    tags = ClusterTaggableManager(through='home.BlogPageTag', blank=True)

    # search_fields = Page.search_fields + [
    #     index.SearchField('summary'),
    #     index.SearchField('body'),
    # ]

    content_panels = Page.content_panels + [
        FieldPanel('date'),
        FieldPanel('cover'),
        FieldPanel('summary'),
        FieldPanel('body', classname='full'),
        FieldPanel('tags'),
    ]

    @property
    def home_page(self):
        return self.get_parent().specific

    def get_context(self, request, *args, **kwargs):
        context = super(BlogPage, self).get_context(request, *args, **kwargs)
        context['home_page'] = self.home_page
        context['post'] = self
        return context


class AboutPage(Page):
    body = RichTextField(blank=True)

    content_panels = Page.content_panels + [
        FieldPanel('body', classname='full')
    ]

    def get_context(self, request, *args, **kwargs):
        context = super(AboutPage, self).get_context(request, *args, **kwargs)
        context['home_page'] = self.get_parent().specific
        context['about'] = self
        return context


class BlogPageTag(TaggedItemBase):
    content_object = ParentalKey('BlogPage', related_name='post_tags')

@register_snippet
class Tag(TaggitTag):
    class Meta:
        proxy = True
